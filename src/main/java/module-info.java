/**
 * module.info for project.
 *
 * @author MicMun
 * @version 1.0, 27.04.21
 */
module miccalendar.main {
   // Require JavaFX
   requires javafx.controls;
   requires javafx.fxml;

   exports de.micmun.java.miccalendar;
   opens de.micmun.java.miccalendar.gui;
   exports de.micmun.java.miccalendar.model;
}
