/*
 * MainAppLauncher
 *
 * Copyright 2021 by MicMun
 */
package de.micmun.java.miccalendar;

/**
 * Launcher for the MainApp (JavaFX in jar file).
 *
 * @author MicMun
 * @version 1.0, 27.04.21
 */
public class MainAppLauncher {
   public static void main(String[] args) {
      MainApp.main(args);
   }
}
