/*
 * MainApp
 *
 * Copyright 2021 by MicMun
 */
package de.micmun.java.miccalendar;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.util.Objects;

/**
 * Main program of the application.
 *
 * @author MicMun
 * @version 1.0, 27.04.21
 */
public class MainApp extends Application {
   static final String APP_NAME = "MicCalendar";
   static final String APP_VERSION = "2.0.0";

   public static void main(String[] args) {
      launch(args);
   }

   @Override
   public void start(Stage primaryStage) throws Exception {
      // Display size
      Rectangle2D screenBounds = Screen.getPrimary().getBounds();

      Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/mainapp.fxml")));
      primaryStage.setTitle(String.format("%s - %s", APP_NAME, APP_VERSION));
      Scene scene = new Scene(root, screenBounds.getWidth() / 2, screenBounds.getHeight() / 2);
      scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/css/days.css"))
            .toExternalForm());
      primaryStage.setScene(scene);
      primaryStage.show();
   }
}
